from app.config import Config
from app.util import Session, get_record
from app.models import Movie, File

from json import dumps

def main():

    session = Session()

    answer = ""

    try:
        record = get_record(session, Config.ID)
    except TypeError:
        answer = 'Invalid id type. Str excepting'
        print(dumps(answer, indent = 4), flush = True)
        session.close()
        return  

    if record is None:
        answer = 'Record not found'   
    else:
        answer = record.to_json()
        #print(dumps(record.to_json(), indent = 4), flush = True)

    print(dumps(answer, indent = 4), flush = True)
    
    session.close()
        
if __name__ == '__main__':
    main()
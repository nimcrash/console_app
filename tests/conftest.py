import pytest
from datetime import datetime
from app.util import Session
from app.models import File, Movie

@pytest.fixture(scope='function')
def test_record():

    session = Session()
    
    movie = Movie(id = "movie_test",
        title = "test",
        description = "test",
        publish = datetime.now(),
        display = True
    )

    session.add(movie);
                
    file = File(id = "file_test",
        path = "test",
        quality = 1,
        movie_id = movie.id
    )

    session.add(file)

    yield {'movie':movie, 'file':file}

    session.rollback()
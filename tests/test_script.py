import pytest
from app.util import Session, get_record

class TestScript:

    def test_get_record(self):

        session = Session()

        record = get_record(session, "1")
        assert record.id == "1"

        record = get_record(session, "-1")
        assert record is None

        with pytest.raises(TypeError):
            get_record(session, 1)

    def test_to_json(self, test_record):

        file = test_record['file']
        movie = test_record['movie']

        assert isinstance(file.to_json(), dict)
        assert isinstance(movie.to_json(), dict)

        movie.file = file

        assert isinstance(movie.to_json(), dict)
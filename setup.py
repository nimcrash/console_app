from setuptools import setup, find_packages

setup(
    name='app',
    version='0.0.1',
    entry_points={
        'console_scripts':[
            'app = app.__main__:main'
        ]
    },
    packages=find_packages()
)
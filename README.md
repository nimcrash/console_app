Напишите консольную утилиту которая принимает на вход идентификатор записи базы данных (postgres),<br> 
генерирует запрос в базу и выводит результат в stdout в виде json строки.<br>

Схема базы (псевдокод)<br>

class File:<br>
id = String<br>
path = String<br>
quality = Int<br>

class Movie:<br>
id = String<br>
title = String<br>
description = String<br>
files = relationship(File)<br>
publish = DateTime<br>
display = Boolean<br>

Скрипт необходимо упаковать в docker контейнер и слинковать с базой данных.<br>

Проверка:<br>
docker-compose up --build -d<br>
docker-compose run -e ID=1 --rm app python script.py

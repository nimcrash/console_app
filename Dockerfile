FROM python:3.7-slim

WORKDIR /src

RUN apt-get update 
#&& apt-get install bash

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY . /src
RUN python setup.py develop

CMD ["app"]
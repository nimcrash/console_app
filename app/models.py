from sqlalchemy import Column, Integer, String, DateTime, Boolean, \
    ForeignKey
from sqlalchemy.orm import relationship
from .base import Base

class File(Base):

    __tablename__ = 'file'

    id = Column(String, primary_key = True)

    path = Column(String)
    quality = Column(Integer)

    movie_id = Column(String, ForeignKey('movie.id'))
    movie = relationship("Movie", back_populates = "file")

    def to_json(self):
        return {
            "path" : self.path,
            "quality": self.quality
        }

class Movie(Base):

    __tablename__ = 'movie'

    id = Column(String, primary_key = True)

    title = Column(String)
    description = Column(String)
    publish = Column(DateTime)
    display = Column(Boolean)

    file = relationship("File", uselist = False, back_populates = "movie")

    def to_json(self):

        file = {}
        if self.file:
            file = self.file.to_json()

        return {
            "title" : self.title,
            "description": self.description,
            "publish": self.publish.strftime('%d-%m-%y'),
            "display": self.display,
            "file": file
        }

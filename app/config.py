from confi import BaseEnvironConfig, ConfigField 

class Config(BaseEnvironConfig):
    
    ID = ConfigField(required = True)
    POSTGRES_USER = ConfigField(required = True)
    POSTGRES_PASSWORD = ConfigField(required = True)
    POSTGRES_DB = ConfigField(required = True)
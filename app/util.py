from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

from .models import Movie, File
from .config import Config

from datetime import datetime, date

engine = create_engine('postgresql+psycopg2://%s:%s@%s/db' %
    (Config.POSTGRES_USER, Config.POSTGRES_PASSWORD, Config.POSTGRES_DB))
Session = sessionmaker(bind=engine)

def create_records(session):

    session = Session()

    record = session.query(Movie).one_or_none()

    if record is None:

        for i in range(5):

            movie = Movie(id = str(i),
                title = "title %s" % i,
                description = "description %s" % i,
                publish = datetime.now(),
                display = i % 2 == 0
            )

            session.add(movie);
                
            file = File(id = str(i),
                path = "path %s" % i,
                quality = i,
                movie_id = movie.id
            )

            session.add(file)

    session.commit()

def get_record(session, id):

    if not type(id) == str:
        #return None
        raise TypeError()
    
    query = session.query(Movie).\
        join(Movie.file).\
        filter_by(id = id)

    return query.one_or_none()
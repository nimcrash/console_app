from .base import Base
from .util import engine, create_records, Session

def main():
    
    Base.metadata.create_all(engine)
    session = Session()
    create_records(session)

if __name__ == '__main__':
    main()